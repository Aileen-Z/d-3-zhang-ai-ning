package ooss;

public class Person {
    public int id;
    public String name;
    public int age;
    public Person(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce(){
        String s = String.format("My name is %s. I am %d years old.",name,age);
        return s;
    }

    //crtl+o
    @Override
    public boolean equals(Object obj) {
        Person person = (Person) obj;
        return this.id==person.id;
    }
}
