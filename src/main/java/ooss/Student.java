package ooss;

public class Student extends Person {
    public Klass klass = new Klass(0);
    public boolean isleader = false;
    public Student(int id, String name, int age){
        super(id, name, age);
    }
    @Override
    public String introduce(){
        String s = "";

        if(isIn(this.klass)&& this.isleader){
            s = String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",name, age, klass.number);
        }else if(this.klass.number != 0 && !this.isleader){
            s = String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",name, age, klass.number);
        }else {
            s = String.format("My name is %s. I am %d years old. I am a student.",name,age);
        }
        return s;
    }

    //crtl+o
    @Override
    public boolean equals(Object obj) {
        Student student = (Student) obj;
        return this.id==student.id;
    }

    public void join(Klass klass){
        this.klass.number = klass.number;
//        klass.classStudentNameList.add(this.name);
    }

    public boolean isIn(Klass klass){
        return this.klass.number == klass.number;
    }



}
