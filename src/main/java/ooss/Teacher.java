package ooss;


import com.sun.tools.javac.util.StringUtils;
//import org.apache.commons.lang.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    List<String> classList = new ArrayList<>();
    public Teacher(int id, String name, int age){
        super(id, name, age);
    }

    @Override
    public boolean equals(Object obj) {
        Teacher teacher = (Teacher) obj;
        return this.id == teacher.id;
    }

    public void assignTo(Klass klass){
        this.classList.add(String.valueOf(klass.number));
//        klass.classTeacherNameList.add(this.name);

    }

    public boolean belongsTo(Klass klass){
        return classList.contains(String.valueOf(klass.number));
    }

    @Override
    public String introduce(){
        String s = "";
        if(this.classList.isEmpty()){
            s = String.format("My name is %s. I am %d years old. I am a teacher.",name,age);
        }else{
            String join = String.join(",",classList);

//java's List call toString removes the [] parentheses at both ends
//Use StringBuffer's append method to concatenate the strings, and then remove the last comma
            StringBuffer classList1 = new StringBuffer();
            for(String str : classList){
                classList1.append(str).append(", ");
            }
            String classList2 = classList1.deleteCharAt(classList1.length()-2).toString().trim();

            s = String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.",name,age,classList2);
        }
        return s;
    }

    public boolean isTeaching(Student student){
        return this.classList.contains(String.valueOf(student.klass.number));
    }
}
