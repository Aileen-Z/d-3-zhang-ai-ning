package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {
    public int number;
    public Klass(int number){
        this.number = number;
    }

    List<String> classTeacherNameList = new ArrayList<>();
    List<String> classStudentNameList = new ArrayList<>();

    @Override
    public boolean equals(Object obj) {
        Klass klass = (Klass) obj;
        return this.number == klass.number;
    }

    public void assignLeader(Student student){
        if(student.klass.number == this.number){
            student.isleader = true;
            for(String classStudentName : classStudentNameList ){
                System.out.println(String.format("I am %s, student of Class 2. I know %s become Leader.",classStudentName,student.name));
            }
            for(String classTeacherName : classTeacherNameList ){
                System.out.println(String.format("I am %s, teacher of Class 2. I know %s become Leader.",classTeacherName,student.name));
            }
        }else{
            System.out.println("It is not one of us.");
        }
    }

    public void attach(Teacher teacher){
        this.classTeacherNameList.add(teacher.name);
    }

    public void attach(Student student){
        this.classStudentNameList.add(student.name);
    }

    public boolean isLeader(Student student){
        return student.isleader;
    }
}
